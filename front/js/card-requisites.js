document.addEventListener('DOMContentLoaded', (event) => {
    document.getElementsByClassName('card-info__wrap')[0].addEventListener('focusout', (event) => {
        if (event.target.classList.contains('card-info__input') && event.target.value) {
            validate(event.target);
        }
    })
    document.getElementsByClassName('card-info__save')[0].addEventListener('click', (event) => {
        event.preventDefault();
        const inputs = document.getElementsByClassName('card-info__wrap')[0].getElementsByClassName('card-info__input');
        const data = {}
        let validAll = true;
        for (let i = 0; i < inputs.length; i++) {
            data[inputs[i].dataset.type] = inputs[i].value;
            validAll &= validate(inputs[i]);
        }
        // const validAll = Array.prototype.every.call(inputs, (input) => {
        //     data[input.dataset.type] = input.value;
        //     return validate(input)
        // });
        if (validAll) {
            fetch('/api/card-requisites', {
                method: 'PUT',
                credentials: 'same-origin',
                headers: {
                    'Content-Type': 'application/json;charset=utf-8'
                },
                body: JSON.stringify(data)
            }).then(() => {
                let newLocation = '/lk';
                if (document.location.href.match(/\?[^\?\#]*#?/)) {
                    const params = {};
                    document.location.href
                        .match(/\?([^\?\#]*)#?/)[1]
                        .split('&')
                        .forEach(string => {
                            const paramArr = string.split('=');
                            params[paramArr[0]] = paramArr[1].replace(/%2F/g, '/')
                        });
                    if ('back_url' in params)
                        newLocation = params['back_url'];
                }
                document.location.href = newLocation;
            })
        }
    })
})

/**
 * 
 * @param {HTMLElement} input 
 */
function validate(input) {
    let valid = false;
    let message = 'Неверно заполнено поле';
    if (input.value.match(/^(\d+(\s*|\/))+$/)) {
        switch (input.dataset.type) {
            case 'number':
                valid = cardNumberValidate(input.value);
                break;
            case 'date':
                valid = cardDateValidation(input.value);
                break;
            case 'cvv':
                valid = cvvValidate(input.value);
                break;
        }
    } else {
        message = input.value ? 'Поле содержит недопустимые символы' : 'Поле не может быть пустым';
    }

    if (valid) {
        input.classList.remove('invalid-input');
        hideError(input)
        switch (input.dataset.type) {
            case 'number':
                input.value = formattingCardNumberValue(input.value);
                break;
            case 'date':
                input.value = formattingDate(input.value);
                break;
        }

    } else {
        renderError(input.dataset.type, message, input);
        input.classList.add('invalid-input');
    }
    return valid;
}

/**
 * 
 * @param {String} cardNumberValue 
 */
function cardNumberValidate(cardNumberValue) {
    const validatingValue = cardNumberValue.replace(/\s+/g, '');
    return (validatingValue.length === 16 || validatingValue.length === 18);
}

/**
 * 
 * @param {String} cardNumberValue 
 */
function formattingCardNumberValue(cardNumberValue) {
    const validatingValue = cardNumberValue.replace(/\s+/g, '');
    let formattingValue = '';
    const lim = Math.ceil(validatingValue.length / 4) - 1;
    for (let i = 0; i < lim; i++) {
        formattingValue += validatingValue.substr(4 * i, 4) + ' ';
    }
    formattingValue += validatingValue.substr(4 * lim);
    return formattingValue;
}

/**
 * 
 * @param {String} cardDateValue 
 */
function formattingDate(cardDateValue) {
    return cardDateValue.substr(0, 2) + '/' + cardDateValue.substr(cardDateValue.length - 2, 2);
}

/**
 * 
 * @param {String} cardNumberValue 
 */
function cardDateValidation(cardDateValue) {
    const validatingCardDateValue = cardDateValue.replace(/\s+|\//g, '');
    const curDate = new Date();
    const curYear = curDate.getFullYear() - 2000,
        curMonth = curDate.getMonth() + 1;
    if (validatingCardDateValue.length === 4) {
        const month = +validatingCardDateValue.substr(0, 2),
            year = +validatingCardDateValue.substr(2, 2);
        if ((month < 13 && month > 0) && ((year > curYear && year <= curYear + 5) || ((year === curYear) && curMonth < month))) {
            return true;
        }
    }
    return false;
}

/**
 * 
 * @param {String} tooltipDataId 
 * @param {String} error 
 * @param {HTMLElement} elem 
 */
function renderError(tooltipDataId, error, elem) {
    const err = elem.parentElement.getElementsByClassName('input-error__wrap')[0];
    err.dataset.id = tooltipDataId;
    const message = err.getElementsByClassName('input-error__message')[0];
    message.textContent = error;
    err.classList.remove('hide');
}

/**
 * 
 * @param {String} tooltipDataId 
 * @param {HTMLElement} elem 
 */
function hideError(elem) {
    const err = elem.parentElement.getElementsByClassName('input-error__wrap')[0];
    err.classList.add('hide');
}
/**
 * 
 * @param {String} cardNumberValue 
 */
function cvvValidate(cardCvvValue) {
    return cardCvvValue.length === 3;
}