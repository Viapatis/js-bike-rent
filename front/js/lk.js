document.addEventListener('DOMContentLoaded', () => {
    document.getElementsByClassName('rented-bikes')[0].addEventListener('click', (event) => {
        if (event.target.classList.contains('return')) {
            returnBike(event.target.dataset.orderId, event.target.parentElement.parentElement, event.currentTarget,);
        }
    })
})
async function returnBike(orderId, target, parentElement) {
    const resp = await fetch(`/api/order/${orderId}`, { method: 'DELETE', credentials: 'same-origin' });
    if (+resp.status.toString()[0] &2) {
        parentElement.removeChild(target);
    }
}