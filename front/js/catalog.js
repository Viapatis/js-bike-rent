'use strict';
function init() {
  const counters = { currentPage: 1, currentId: 0 };
  loadCatalog(counters);
  const isLogin = checkLogin();
  document.getElementsByClassName('load')[0].onclick = () => {
    loadCatalog(counters);
  }
  document.getElementById('bikeList').addEventListener('click', (event) => {
    if (event.target.classList.contains('bike-item__btn')) {
      if (isLogin) {
        createOrder(event.target.dataset.bikeId);
      } else {
        document.location.href = "/login";
      }
    }
  })
}
async function loadCatalog(counters) {
  disableButtonLoadMore();
  let id = getPointId();
  let showBut = false;
  if (id === 'catalog') {
    const pointers = await api.getPointers();
    const ids = pointers.map(pointer => pointer._id);
    id = ids[counters.currentId];
    showBut = counters.currentId !== ids.length - 1;
  }
  const bikes = await getBikes(id, counters.currentPage);
  appendCatalog(bikes.bikesList);
  showButtonLoadMore(bikes.hasMore || showBut);
  if (!bikes.hasMore) {
    counters.currentId++;
    counters.currentPage = 0;
  }
  enableButtonLoadMore();
  counters.currentPage++;
}

async function getBikes(id, page) {
  const resp = await fetch(`api/catalog/${id}?page=${page}#`, { method: 'GET', credentials: 'same-origin' });
  return resp.json();
}

function appendCatalog(items) {
  const bikeList = document.getElementById('bikeList');
  items.forEach(item => {
    const bikeItem = document.createElement('div');
    bikeItem.classList.add('bike-item');
    bikeItem.innerHTML =
      `<img class='bike-item__img' src='../images/${item.img}'/>
    <p class='bike-item__name'>${item.name}</p>
    <p class='bike-item__cost'>Стоимость за час - ${item.cost * 60}</p>
    <button class='bike-item__btn button' data-bike-id='${item._id}'>Арендовать</button>`
    bikeList.appendChild(bikeItem);
  });
  // отрисуй велосипеды из items в блоке <div id="bikeList">
}
function checkLogin() {
  return document.getElementsByClassName('header__link')[0].dataset.isLogin === 'true';
}

function createOrder(bikeId) {
  document.location.href = `/order/${bikeId}`;
}
function showButtonLoadMore(hasMore) {
  document.getElementById("loadMore").classList[hasMore ? 'remove' : 'add']('hidden');
  // если hasMore == true, то показывай кнопку #loadMore
  // иначе скрывай
}

function disableButtonLoadMore() {
  // заблокируй кнопку "загрузить еще"
  document.getElementById("loadMore").setAttribute('disabled', 'true');
}

function enableButtonLoadMore() {
  // разблокируй кнопку "загрузить еще"
  document.getElementById("loadMore").setAttribute('disabled', 'false');
}

function getPointId() {
  // сделай определение id выбранного пункта проката
  return document.URL.split('/').reverse()[0];
}


document.addEventListener('DOMContentLoaded', init)
