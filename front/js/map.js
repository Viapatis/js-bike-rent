
document.addEventListener('DOMContentLoaded', () => {
    ymaps.ready(initMap);
})

async function initMap() {
    try {
        const location = await ymaps.geolocation.get();
        const yanMap = await new ymaps.Map("map", {
            center: location.geoObjects.position,
            zoom: 11
        });
        const points = await api.getPointers();
        const geoObjects = [];
        const clusterer = new ymaps.Clusterer({ clusterDisableClickZoom: true });
        points.forEach(point => {
            const obj = new ymaps.GeoObject({
                geometry: {
                    type: "Point",
                    coordinates: point.coordinates
                },
                properties: {
                    clusterCaption: `Пункт выдачи по адресу ${point.address}`,
                    balloonContentBody: ''
                }
            })
            obj.events.add('click', () => {
                yanMap.balloon.open(obj.geometry._coordinates, {
                    contentHeader: 'Аренда велосипедов',
                    contentBody: `Пункт выдачи по адресу ${point.address}`,
                });
            })
            geoObjects.push(obj);
        });
        yanMap.geoObjects.add(clusterer.add(geoObjects));
    } catch (err) {
        console.log(err.message);
    }
}